const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/user');
const bcrypt = require('bcrypt');
const FbStrategy = require('passport-facebook').Strategy;
const passport = require('passport');

function config () {
  // serialize
  passport.serializeUser((user, cb) => {
    cb(null, user._id);
  });

  // deserialize
  passport.deserializeUser((id, cb) => {
    User.findById(id, (err, user) => {
      if (err) { return cb(err); }
      cb(null, user);
    });
  });

  // local strategy for passport
  passport.use(new LocalStrategy((username, password, next) => {
    User.findOne({ username }, (err, user) => {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next(null, false, { message: 'Incorrect username or password' });
      }
      if (!bcrypt.compareSync(password, user.password)) {
        return next(null, false, { message: 'Incorrect username or password' });
      }

      return next(null, user);
    });
  }));

  // facebook strategy for passport
  passport.use(new FbStrategy({
    clientID: '',
    clientSecret: '',
    callbackURL: '/auth/facebook/callback',
    profileURL: 'https://graph.facebook.com/v2.5/me?fields=name,email',
    profileFields: ['id', 'displayName', 'name', 'gender', 'picture.type(large)']
  }, (accessToken, refreshToken, profile, done) => {
    User.findOne({ 'facebookId': profile.id }, (err, user) => {
      if (err) {
        return done(err);
      }
      if (user) {
        return done(null, user);
      }

      const newUser = new User();
      newUser.facebookId = profile.id;
      newUser.name = profile.displayName;
      newUser.smPicPath = profile.photos ? profile.photos[0].value : '/img/faces/unknown-user-pic.jpg';

      newUser.save((err) => {
        if (err) {
          return done(err);
        }
        done(null, newUser);
      });
    });
  }));
};

module.exports = config;
