'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

// schema for user
const userSchema = new Schema({
  username: String,
  password: String,
  name: String,
  myGames: [],
  myFriends: [{
    type: ObjectId,
    ref: 'User'
  }],
  friendNotification: [{
    type: ObjectId,
    ref: 'User'
  }],
  sendFriendNotification: [{
    type: ObjectId,
    ref: 'User'
  }],
  reviews: [{
    owner: {
      type: ObjectId,
      ref: 'User'
    },
    createdAt: String,
    content: String
  }],
  receivedMessage: [{
    owner: {
      type: ObjectId,
      ref: 'User'
    },
    createdAt: String,
    content: String,
    unreaded: Boolean
  }],
  reply: [{
    messageId: String,
    owner: {
      type: ObjectId,
      ref: 'User'
    },
    createdAt: String,
    content: String,
    unreaded: Boolean
  }],
  sentMessage: [{
    owner: {
      type: ObjectId,
      ref: 'User'
    },
    createdAt: String,
    content: String,
    unreaded: Boolean
  }],
  picPath: String,
  smPicPath: String,
  facebookId: String,
  rateUser: [{
    owner: {
      type: ObjectId,
      ref: 'User'
    },
    createdAt: Date,
    rate: Number,
    enum: ['1', '2', '3', '4', '5'],
    default: 0
  }],
  rateAvg: Number

}, {
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }
});

const User = mongoose.model('User', userSchema);

module.exports = User;
