const igdb = require('igdb-api-node').default;
const express = require('express');
const router = express.Router();
const User = require('../models/user');

// API database client

// const client = igdb('83c7bb9acd6cca79c5e6f775348b759e');
const client = igdb('0b9966efd427332243cb58c2c285520a');

// display news

router.get('/news', (req, res, next) => {
  client.pulses({

    fields: '*' // Return all fields
    // fields: ['name', 'summary', 'rating', 'cover'],

  }).then(response => {
    res.json(response);
  }).catch(error => {
    res.status(500).json(error);
  });
});

// display story

router.get('/news/:id', (req, res, next) => {
  client.pulses({
    ids: [
      req.params.id
    ],
    // fields: '*', // Return all fields
    fields: '*',
    limit: 20, // Limit to 5 results
    offset: 15 // Index offset for results
  }).then(response => {
    res.json(response);
  }).catch(error => {
    res.status(500).json(error);
  });
});

// display all companies

router.get('/companies', (req, res, next) => {
  client.companies({

    fields: '*' // Return all fields
    // fields: ['name', 'summary', 'rating', 'cover'],

  }).then(response => {
    res.json(response);
  }).catch(error => {
    res.status(500).json(error);
  });
});

// display all platforms

router.get('/platforms', (req, res, next) => {
  client.platforms({

    // fields: '*' // Return all fields
    fields: ['id', 'games', 'name'],
    limit: 50, // Limit to 5 results
    offset: 15 // Index offset for results
  }).then(response => {
    res.json(response);
  }).catch(error => {
    res.status(500).json(error);
  });
});

// display all games

router.get('/games', (req, res, next) => {
  client.games({
    filters: {
      'release_dates.date-gt': '2010-12-31',
      'rating-gt': '90',
      'total_rating_count-gt': '100'
    },
    order: 'rating:desc',
    fields: '*', // Return all fields
    // fields: ['name', 'summary', 'rating', 'cover'],
    limit: 20, // Limit to 5 results
    offset: 15 // Index offset for results
  }).then(response => {
    res.json(response);
  }).catch(error => {
    res.status(500).json(error);
  });
});

// display particular game

router.get('/games/:id', (req, res, next) => {
  client.games({
    ids: [
      req.params.id
    ],
    // fields: '*', // Return all fields
    fields: ['name', 'summary', 'rating', 'cover'],
    limit: 20, // Limit to 5 results
    offset: 15 // Index offset for results
  }).then(response => {
    res.json(response);
  }).catch(error => {
    res.status(500).json(error);
  });
});

// display favorite games

router.post('/userGames', (req, res, next) => {
  console.log(req.body.myGames);
  if (req.body.myGames.length !== 0) {
    client.games({
      ids: [
        req.body.myGames
      ],
      // fields: '*', // Return all fields
      fields: ['name', 'summary', 'rating', 'cover'],
      limit: 20, // Limit to 5 results
      offset: 15 // Index offset for results
    }).then(response => {
      res.json(response);
    }).catch(error => {
      res.status(500).json(error);
    });
  }
});

// filter games

router.post('/games/search', (req, res, next) => {
  console.log(req.body.search);
  client.games({
    search: req.body.search,
    order: 'rating:desc',
    // fields: '*', // Return all fields
    fields: ['name', 'summary', 'rating', 'cover'],
    limit: 20, // Limit to 5 results
    offset: 15 // Index offset for results
  }).then(response => {
    res.json(response);
  }).catch(next);
});

// add game to favorite

router.post('/games/fav/:id', (req, res, next) => {
  if (!req.user) {
    return res.status(401).json({error: 'unauthorized'});
  }
  const userId = req.user.id;
  const gameFavId = req.params.id;

  User.findByIdAndUpdate(userId, { $push: { myGames: gameFavId } })
    .then((user) => {
      return res.json(user);
    }).catch(err => {
      return next(err);
    });
});

module.exports = router;
