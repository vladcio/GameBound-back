const express = require('express');
const router = express.Router();
const User = require('../models/user');
const multer = require('multer');

const upload = multer({ dest: './public/uploads/' });

// get all users

router.get('/users', (req, res, next) => {
  User.find({})
    .then((users) => res.json(users))
    .catch(next);
});

// filter users

router.post('/users/search', (req, res, next) => {
  const bodysearch = req.body.search;
  User.find({name: {$regex: bodysearch, $options: 'i'}})
    .then((users) => res.json(users))
    .catch(next);
});

router.get('/users/:id', (req, res, next) => {
  User.findById(req.params.id)
    .populate('myFriends')
    .populate('reviews.owner')
    .populate('friendNotification')
    .populate('receivedMessage.owner')
    .populate('reply.owner')
    .exec((err, user) => {
      if (err) { return res.json(err).status(500); }
      if (!user) { return res.json(err).status(404); }
      return res.json(user);
    });
});

// message user

router.post('/messages/:id', (req, res, next) => {
  const userId = req.user.id;
  if (!req.user) {
    res.redirect('/auth/login');
  }
  const date = new Date();
  const dateFormated = date.toDateString();
  const userMessagedId = req.params.id;
  const sentmessageData = {
    owner: req.user.id,
    content: req.body.messageContent,
    createdAt: dateFormated,
    unreaded: true
  };

  User.findByIdAndUpdate(userId, { $push: { sentMessage: sentmessageData } })
    .then((user) => {
      return User.findByIdAndUpdate(userMessagedId, { $push: { receivedMessage: sentmessageData } });
    })
    .then((friend) => {
      return res.json(friend);
    }).catch(err => {
      return next(err);
    });
});

router.post('/reply/:id', (req, res, next) => {
  const userId = req.user.id;
  if (!req.user) {
    res.redirect('/auth/login');
  }

  const date = new Date();
  const dateFormated = date.toDateString();
  const userMessagedId = req.params.id;
  const sentmessageData = {
    messageId: req.body.messageId,
    owner: req.user.id,
    content: req.body.content,
    createdAt: dateFormated,
    unreaded: true

  };

  User.findByIdAndUpdate(userId, { $push: { sentMessage: sentmessageData } })
    .then((user) => {
      return User.findByIdAndUpdate(userMessagedId, { $push: { reply: sentmessageData } });
    })
    .then((friend) => {
      return res.json(friend);
    }).catch(err => {
      return next(err);
    });
});

// upload

router.post('/upload', upload.single('uploadedFile'), (req, res, next) => {
  if (!req.user) {
    return res.status(401).json({error: 'unauthorized'});
  }
  const userId = req.user.id;
  const updatePic = {
    picPath: `/uploads/${req.file.filename}`
  };

  User.findByIdAndUpdate(userId, updatePic)
    .then((user) => {
      return res.json(user);
    }).catch(err => {
      return next(err);
    });
});

// send friend notification

router.post('/fav/:id', (req, res, next) => {
  const userId = req.user.id;
  if (!req.user) {
    res.redirect('/auth/login');
  }
  const userFavId = req.params.id;

  User.findByIdAndUpdate(userId, { $push: { sendFriendNotification: userFavId } })
    .then((user) => {
      return User.findByIdAndUpdate(userFavId, { $push: { friendNotification: userId } });
    })
    .then((friend) => {
      return res.json(friend);
    }).catch(err => {
      return next(err);
    });
});

// accept friend notification

router.post('/accept/:id', (req, res, next) => {
  const userId = req.user.id;
  if (!req.user) {
    res.redirect('/auth/login');
  }
  const userFavId = req.params.id;

  User.findByIdAndUpdate(userId, { $push: { myFriends: userFavId } })
    .then((user) => {
      return User.findByIdAndUpdate(userId, { $pop: { friendNotification: userFavId } });
    })
    .then((user) => {
      return User.findByIdAndUpdate(userFavId, { $push: { myFriends: userId } });
    })
    .then((user) => {
      return User.findByIdAndUpdate(userFavId, { $pop: { sendFriendNotification: userId } });
    })
    .then((friend) => {
      return res.json(friend);
    }).catch(err => {
      return next(err);
    });
});

// reject friend notification

router.post('/reject/:id', (req, res, next) => {
  const userId = req.user.id;
  if (!req.user) {
    res.redirect('/auth/login');
  }
  const userFavId = req.params.id;

  User.findByIdAndUpdate(userId, { $pop: { friendNotification: userFavId } })
    .then((user) => {
      return User.findByIdAndUpdate(userFavId, { $pop: { sendFriendNotification: userId } });
    })
    .then((friend) => {
      return res.json(friend);
    }).catch(err => {
      return next(err);
    });
});

// review user

router.post('/review/:id', (req, res, next) => {
  const userId = req.params.id;
  if (!req.user) {
    res.redirect('/auth/login');
  }
  const date = new Date();
  const dateFormated = date.toDateString();
  const reviewData = {
    owner: req.user.id,
    content: req.body.content,
    createdAt: dateFormated
  };

  User.findByIdAndUpdate(userId, { $push: { reviews: reviewData } })
    .then((user) => {
      return res.redirect('/users/' + userId);
    }).catch(err => {
      return next(err);
    });
});

// rate user

router.post('/rate/:id', (req, res, next) => {
  const userId = req.params.id;
  if (!req.user) {
    res.redirect('/auth/login');
  }
  const rateData = {
    owner: req.user.id,
    rate: req.body.rate,
    createdAt: new Date()
  };
  const rateAvg = req.body.rateAvg;

  User.findByIdAndUpdate(userId, { $push: { rateUser: rateData } }, {new: true})
    .then((user) => {
      return User.findByIdAndUpdate(userId, {rateAvg: rateAvg}, {new: true});
    })
    .then((user) => {
      return res.json(user);
    }).catch(err => {
      return next(err);
    });
});

module.exports = router;
