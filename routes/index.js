var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', (req, res, next) => {
  res.json({welcome: 'gameBound API'});
});

module.exports = router;
