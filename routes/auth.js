const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();
const passport = require('passport');
const ensureLogin = require('connect-ensure-login');

const User = require('../models/user');

// check which user is logged in

router.get('/me', (req, res, next) => {
  if (req.user) {
    res.json(req.user);
  } else {
    res.status(404).json({error: 'not-found'});
  }
});

// login local user

router.post('/login', (req, res, next) => {
  passport.authenticate('local', (err, theUser, failureDetails) => {
    const username = req.body.username;
    const password = req.body.password;
    if (err) {
      res.status(500).json({ message: 'Something went wrong' });
      return;
    }

    if (req.user) {
      return res.status(401).json({error: 'Unauthorized'});
    }
    if (!username || !password) {
      return res.status(422).json({error: 'Insert username and password'});
    }

    if (!theUser) {
      res.status(401).json(failureDetails);
      return;
    }

    req.login(theUser, (err) => {
      if (err) {
        res.status(500).json({ message: 'Something went wrong' });
        return;
      }

      // We are now logged in (notice req.user)
      res.status(200).json(req.user);
    });
  })(req, res, next);
});

// signup local

router.post('/signup', (req, res, next) => {
  const username = req.body.username;
  const password = req.body.password;
  const name = req.body.name;

  if (req.user) {
    return res.status(401).json({error: 'unauthorized'});
  }

  if (!username || !password || !name) {
    res.status(400).json({ error: 'All fields are mandatory' });
    return;
  }

  User.findOne({ username }, '_id', (err, foundUser) => {
    if (foundUser) {
      res.status(400).json({ error: 'The username already exists' });
      return;
    }

    const salt = bcrypt.genSaltSync(10);
    const hashPass = bcrypt.hashSync(password, salt);

    const theUser = new User({
      name,
      username,
      password: hashPass
    });

    theUser.save((err) => {
      if (err) {
        res.status(400).json({ error: 'Something went wrong' });
        return;
      }

      req.login(theUser, (err) => {
        if (err) {
          res.status(500).json({ error: 'Something went wrong' });
          return;
        }

        res.status(200).json(req.user);
      });
    });
    if (err) {
      res.status(500).json({ error: 'Something went wrong' });
    }
  });
});

// logout

router.post('/logout', (req, res, next) => {
  req.logout();
  res.status(200).json({ message: 'Success' });
});

// ensure user is logged
router.get('/', ensureLogin.ensureLoggedIn(), (req, res) => {
  res.render('books/books-list', { user: req.user });
});

// facebook log in
router.get('/facebook', passport.authenticate('facebook'));
router.get('/facebook/callback', passport.authenticate('facebook', {
  successRedirect: process.env.CLIENT_URL,
  failureRedirect: process.env.CLIENT_URL + '/error'
}));

module.exports = router;
